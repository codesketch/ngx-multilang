//    Copyright 2018 Quirino Brizi <quirino.brizi@gmail.com>
// 
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
// 
//        http://www.apache.org/licenses/LICENSE-2.0
// 
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.

import { AbstractMultilangComponentProvider } from '../../../../src/main/ts/abstract.multilang.component.provider';
import { Lang, MultilangInterface } from '../../../../src/main/ts/multilang.interface';
import { Type, Component } from '@angular/core';
import { HomePageEnComponent } from './components/home.page.en.component';
import { HomePageItComponent } from './components/home.page.it.component';

@Component({
  selector: 'app-how-it-works',
  entryComponents: [
    HomePageEnComponent,
    HomePageItComponent
  ],
  template: '<ng-template multi-lang></ng-template>'
})
export class HomePageComponentProvider extends AbstractMultilangComponentProvider {

  provideComponentForLanguage(language: Lang): Type<MultilangInterface> {
    if (language == Lang.EN) {
      return HomePageEnComponent;
    }
    return HomePageItComponent;
  }
}