import { Directive, ViewContainerRef } from '@angular/core';

@Directive({
  selector: '[multi-lang]',
})
export class MultilangDirective {
  constructor(public viewContainerRef: ViewContainerRef) { }
}