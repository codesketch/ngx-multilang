//    Copyright 2018 Quirino Brizi <quirino.brizi@gmail.com>
// 
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
// 
//        http://www.apache.org/licenses/LICENSE-2.0
// 
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.

import { ComponentFactoryResolver, ViewChild, ViewContainerRef, ComponentFactory, Type, ComponentRef, AfterViewInit } from "@angular/core";
import { TranslateService } from "@ngx-translate/core";
import { MultilangDirective } from "./multilang.directive";
import { Lang, MultilangInterface } from "./multilang.interface";

export abstract class AbstractMultilangComponentProvider implements AfterViewInit {

  @ViewChild(MultilangDirective) pageHost!: MultilangDirective;

  constructor(protected componentFactoryResolver: ComponentFactoryResolver,
    protected translateService: TranslateService) { }

  ngAfterViewInit(): void {
    this.loadComponent();
  }

  protected loadComponent(): void {
    const currentLanguage: Lang = this.getCurrentLanguage();
    const componentFactory: ComponentFactory<MultilangInterface> =
      this.componentFactoryResolver.resolveComponentFactory(this.provideComponentForLanguage(currentLanguage));

    const viewContainer: ViewContainerRef = this.pageHost.viewContainerRef;
    viewContainer.clear();
    const component: ComponentRef<MultilangInterface> = viewContainer.createComponent(componentFactory);
    setTimeout(() => { this.translateService.use(component.instance.language().toString()); }, 50);
  }

  protected abstract provideComponentForLanguage(language: Lang): Type<MultilangInterface>;

  protected getCurrentLanguage(): Lang {
    const lang = this.translateService.currentLang ? this.translateService.currentLang.toUpperCase() : "EN";
    return (<any>Lang)[lang] || Lang.EN;
  }
}