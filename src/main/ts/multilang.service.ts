//    Copyright 2018 Quirino Brizi <quirino.brizi@gmail.com>
// 
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
// 
//        http://www.apache.org/licenses/LICENSE-2.0
// 
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.

import { Inject } from "@angular/core";
import { NavigationExtras, Router } from "@angular/router";
import { LocalizeParser, LocalizeRouterSettings } from "localize-router";

export class MultilangService {


  constructor(@Inject(LocalizeParser) protected parser: LocalizeParser, @Inject(LocalizeRouterSettings) protected settings: LocalizeRouterSettings, @Inject(Router) protected router: Router) {
  }


  swtchLanguage(language: string, options: NavigationExtras, useNavigateMethod: Boolean) {
    if (language != this.parser.currentLang) {
      const currentUrl: string = this.router.url;
      const newRoute: string = this.parser.translateRoute(currentUrl.replace(this.parser.currentLang, language));
      this.parser.translateRoutes(language).subscribe(() => {
        // reset the router config so that routes are dynamically loaded.
        this.router.resetConfig(this.parser.routes);

        const extras = Object.assign({ skipLocationChange: false }, options);
        if (useNavigateMethod) {
          this.router.navigate([newRoute], extras);
        } else {
          this.router.navigateByUrl(newRoute, extras);
        }
      });
    }
  }
}