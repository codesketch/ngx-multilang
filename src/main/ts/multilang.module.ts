//    Copyright 2018 Quirino Brizi <quirino.brizi@gmail.com>
// 
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
// 
//        http://www.apache.org/licenses/LICENSE-2.0
// 
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.

import { CommonModule } from '@angular/common';
import { APP_INITIALIZER, ModuleWithProviders, NgModule, NgModuleFactoryLoader, Optional, SkipSelf, InjectionToken } from "@angular/core";
import { RouterModule, Routes } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { ALWAYS_SET_PREFIX, CACHE_MECHANISM, CACHE_NAME, DEFAULT_LANG_FUNCTION, DummyLocalizeParser, getAppInitializer, LocalizeParser, LocalizeRouterConfig, LocalizeRouterConfigLoader, LocalizeRouterPipe, LocalizeRouterService, LocalizeRouterSettings, LOCALIZE_ROUTER_FORROOT_GUARD, ParserInitializer, RAW_ROUTES, USE_CACHED_LANG } from 'localize-router';
import { MultilangDirective } from "./multilang.directive";
import { MultilangService } from './multilang.service';

/**
 * @see https://github.com/Greentube/localize-router/blob/master/src/localize-router.module.ts
 */

export declare const MULTILANG_ROUTER_FORROOT_GUARD: InjectionToken<MultilangRouterModule>;

@NgModule({
  imports: [CommonModule, RouterModule, TranslateModule],
  exports: [MultilangDirective, LocalizeRouterPipe],
  declarations: [MultilangDirective, LocalizeRouterPipe]
})
export class MultilangRouterModule {

  /**
   * Define the Multilang with all directives and providers.
   * @param routes the application routes
   * @param config the localized routing configuration
   * @see https://github.com/Greentube/localize-router/blob/master/src/localize-router.module.ts
   */
  static forRoot(routes: Routes, config: LocalizeRouterConfig = {}): ModuleWithProviders {
    return {
      ngModule: MultilangRouterModule,
      providers: [
        {
          provide: MULTILANG_ROUTER_FORROOT_GUARD, useFactory: provideForRootGuard, deps: [[MultilangRouterModule, new Optional(), new SkipSelf()]]
        },
        { provide: USE_CACHED_LANG, useValue: config.useCachedLang },
        { provide: ALWAYS_SET_PREFIX, useValue: config.alwaysSetPrefix },
        { provide: CACHE_NAME, useValue: config.cacheName },
        { provide: CACHE_MECHANISM, useValue: config.cacheMechanism },
        { provide: DEFAULT_LANG_FUNCTION, useValue: config.defaultLangFunction },
        LocalizeRouterSettings,
        config.parser || { provide: LocalizeParser, useClass: DummyLocalizeParser },
        {
          provide: RAW_ROUTES, multi: true, useValue: routes
        },
        MultilangService,
        LocalizeRouterService,
        ParserInitializer,
        { provide: NgModuleFactoryLoader, useClass: LocalizeRouterConfigLoader },
        {
          provide: APP_INITIALIZER, multi: true, useFactory: getAppInitializer, deps: [ParserInitializer, LocalizeParser, RAW_ROUTES]
        }
      ]
    };
  }

  /**
     * Creates a module with all the routes.
     * @param routes the module routes
     * @see https://github.com/Greentube/localize-router/blob/master/src/localize-router.module.ts
     */
  static forChild(routes: Routes): ModuleWithProviders {
    return {
      ngModule: MultilangRouterModule,
      providers: [
        {
          provide: RAW_ROUTES, multi: true, useValue: routes
        }
      ]
    };
  }
}

/**
 * @param multilangRouterModule
 * @returns {string}
 */
export function provideForRootGuard(multilangRouterModule: MultilangRouterModule): string {
  if (multilangRouterModule) {
    throw new Error(
      `MultilangRouterModule.forRoot() called twice. Lazy loaded modules should use MultilangRouterModule.forChild() instead.`);
  }
  return 'guarded';
}