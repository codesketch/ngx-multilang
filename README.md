## ngx-multilang

A simple framework for simplified multi-language web-application development.

ngx-multilang approaches multi-language web-application development leveraging dynamic component loading feature provided by Angualar, it is for now tested with angular 5 only, and integrates with 
 - [localize-router](https://github.com/Greentube/localize-router) library to provide a simple yet powerful dynamic routing,
 - [ngx-translate](https://github.com/ngx-translate/core) library to provide language translation functionalities

### How do I install it
```bash
npm install --save ngx-multilang
```

### How do I use it
ngx-multilang uses module approach as such ot can be enabled as below
```javascript
const routes: Routes = [...]

@NgModule({
  imports: [
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: (http: HttpClient) => { return new TranslateHttpLoader(http, '/assets/locales/', '.json'); },
        deps: [HttpClient]
      }
    }),
    MultilangModule.forRoot(routes, {
      parser: {
        provide: LocalizeParser,
        useFactory: (translate, location, settings, http) =>
          new LocalizeRouterHttpLoader(translate, location, settings, http),
        deps: [TranslateService, Location, LocalizeRouterSettings, HttpClient]
      }
    }),
    RouterModule.forRoot(routes)],
  exports: [TranslateModule, LocalizeRouterModule, RouterModule]
})
export class AppRoutingModule {

}
```
and your AppModule
```javascript
@NgModule({
  declarations: [
    ...
    MultilangDirective
  ],
  imports: [
    ...
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
```
at this point you can swithch language for your application
```javascript
@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css'],
  providers: [Location]
})
export class NavComponent {

  constructor(private multilangService: MultilangService) { }

  switchLanguage(language: string) {
    this.multilangService.swtchLanguage(language, {}, true);
  }

}
```
and enable dynamic component loading as described on [this example](https://gitlab.com/codesketch/ngx-multilang/tree/master/example/src).